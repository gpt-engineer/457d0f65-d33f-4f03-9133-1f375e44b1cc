// Select the record button and library
const recordButton = document.getElementById('recordButton');
const library = document.getElementById('library');

// Initialize MediaRecorder
let mediaRecorder;
let chunks = [];
let audioBlob;

// Handle button click
recordButton.addEventListener('click', function() {
    // If not currently recording
    if (!mediaRecorder || mediaRecorder.state === 'inactive') {
        // Request access to the microphone
        navigator.mediaDevices.getUserMedia({ audio: true })
            .then(stream => {
                // Create a new MediaRecorder
                mediaRecorder = new MediaRecorder(stream);

                // Start recording
                mediaRecorder.start();

                // Update button text
                recordButton.innerHTML = '<i class="fas fa-stop"></i> Stop';

                // Handle dataavailable event
                mediaRecorder.addEventListener('dataavailable', function(e) {
                    chunks.push(e.data);
                });

                // Handle stop event
                mediaRecorder.addEventListener('stop', function() {
                    // Create a blob from the chunks
                    audioBlob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });

                    // Reset chunks for next recording
                    chunks = [];

                    // Prompt the user to name the file
                    let fileName = prompt('Please name your file:');

                    // Create a new audio element
                    let audio = document.createElement('audio');
                    audio.controls = true;
                    audio.src = URL.createObjectURL(audioBlob);

                    // Append the audio element to the library
                    library.innerHTML += `
                        <div class="bg-white p-4 rounded shadow mb-2">
                            <h3 class="text-lg mb-1">${fileName}</h3>
                            <div>${audio.outerHTML}</div>
                        </div>
                    `;

                    // Update button text
                    recordButton.innerHTML = '<i class="fas fa-microphone"></i> Record';
                });
            })
            .catch(err => console.log(err));
    } else {
        // Stop recording
        mediaRecorder.stop();
    }
});
